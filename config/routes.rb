Rails.application.routes.draw do
  resources :items
  root 'welcome#index'
  get 'pruebas' => 'welcome#pruebas'
end
