require 'test_helper'

class ItemTest < ActiveSupport::TestCase
  def setup
    @item = Item.new(title: "Un título", 
      comun: "aldfkjasdfajkjdflasdkjfsdsadf",
      python: "trewtrewt rtwe treter terwtt etwertertwe",
      ruby: "fdgsd fdgsdfg dfg dgsd dfsgsdf gsdg dfsgsdf gfd")
  end

  test "should be valid" do
    assert @item.valid?
  end
  
  test "debería ser inválido sin title" do
    item2 = Item.new(title: " ", 
      comun: "aldfkjasdfajkjdflasdkjfsdsadf")
    refute item2.valid?
  end
  
  test "debería ser inválido sin comun" do
    item2 = Item.new(title: "dsfasdf fdsafasdf")
    refute item2.valid?
  end
  
end
