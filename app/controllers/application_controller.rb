class ApplicationController < ActionController::Base
  before_filter :time_set
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def time_set
    @hora = Time.now
    @horajs = @hora.strftime("%m/%d/%Y %I:%M %p UTC")
    @admin = params["admin"]
  end

end
