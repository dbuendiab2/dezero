// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
//= require turbolinks
//= require_tree .

 function display_c(){
  var refresh=1000; // Refresh rate in milli seconds
  var mytime=setTimeout('display_ct()',refresh);
  }

  var x0 = new Date();
  
  function display_ct() {
  var strcount
  var x = new Date()
  document.getElementById('ct').innerHTML = "Acceso página: " + formatDate(x0) + ", tiempo transcurrido: " + formatInterval(x-x0);
  var tt=display_c();
  }
  
  function formatDate(date) {
    var year = date.getFullYear(),
        month = date.getMonth() + 1, // months are zero indexed
        day = date.getDate(),
        hour = date.getHours(),
        minute = date.getMinutes(),
        second = date.getSeconds(),
        hourFormatted = hour, //% 12 || 12, // hour returned in 24 hour format
        minuteFormatted = minute < 10 ? "0" + minute : minute,
        secondFormatted = second < 10 ? "0" + second : second
        //morning = hour < 12 ? "AM" : "PM";

    return day + "/" + month + "/" + year + " " + hourFormatted + ":" +
            minuteFormatted + ":" + secondFormatted; //+ morning;
}

  function formatInterval(milisegundos) {
    var second, minute=0, hour=0;
    second = parseInt(milisegundos / 1000);
    if (second > 60) {
      minute = parseInt(second / 60);
      second = second % 60;
      if (minute > 60) {
        hour = parseInt(minute / 60);
        minute = minute % 60;
      }
    }
    if(hour > 0) {
      return hour + ":" + (minute < 10? "0": "") + minute + ":" + (second < 10? "0": "") + second + " (hh:mm:ss)"
    } else if(minute > 0) {
      return  (minute < 10 ? "0": "") + minute + ":" + (second < 10? "0": "") + second + " (mm:ss)"
    }
    return second +" segundos"
    
  }